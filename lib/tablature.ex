defmodule Tablature do
  def parse(tab) do
    String.split(tab)
    |> Enum.map(fn line -> parse_line(line) end)
    |> List.flatten
    #|> List.keysort(1)
    |> Enum.group_by(&elem(&1, 1))
    |> Enum.sort_by(&elem(&1, 0))
    |> Enum.map(fn {_posicion, notas} ->
       notas
       |> Enum.map(fn {caracter, posicion} -> Tuple.to_list({caracter, posicion}) end)
       |> Enum.uniq
       |> Enum.map(fn list -> List.to_tuple(list) end)
    end)
    |> Enum.map(fn list ->
       if (length(list) > 1) do
        Enum.map(list, fn {caracter, posicion} ->
           if (caracter == "-") do
              List.delete(list, {caracter, posicion})
           end
        end)
      else
        list
      end
    end)
    |> Enum.map(fn list -> Enum.filter(list, & !is_nil(&1)) end)
    |> Enum.map(fn list -> List.flatten(list) end)
    |> Enum.map(fn list ->
        Enum.map(list, fn {_caracter, posicion} -> {posicion, list} end)
        |> Enum.uniq
    end)
    |> List.flatten
    |> Enum.map(fn {_posicion, notas} ->
      notas
      |> Enum.map(fn {caracter, _posicion} -> caracter end)
      |> Enum.join("/")
  end)
    |> Enum.join("")
    |> String.replace("---", " _ ")
    |> String.replace("--", "-")
    |> String.replace_prefix("-", "")
    |> String.replace_suffix("-", "")
    |> String.replace("-", " ")
  end
  def parse_line(line) do
    note = String.first(line)

    String.codepoints(line)
    |> Enum.with_index()
    |> Enum.filter(fn {caracter, _posicion} -> Regex.match?(~r/[0-9]/, caracter) || caracter=="-" end)
    |> Enum.map(fn {caracter, posicion} ->
       if (caracter == "-") do
        {caracter, posicion}
       else
        {note <> caracter, posicion}
       end
       end)
  end

end
